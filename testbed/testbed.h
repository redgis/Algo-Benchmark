/*
 * testbed.h
 *
 *  Created on: May 12, 2010
 *      Author: regis
 */

#ifndef TESTBED_H_
#define TESTBED_H_

// C++
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>

// ANSI C
#include <stdlib.h>
#include <string.h>

// Linux headers
#include <dlfcn.h>
#include <sys/time.h>

// Stats
#include <gsl/gsl_math.h>
#include <gsl/gsl_statistics.h>

using namespace std;

// Module Interface
extern void (* tbInitialize) ();
extern void * (* tbCreateData) (int argc, char * argv[]);
extern bool (* tbValidate) (void *);
extern void (* tbUsage) ();
extern void * (* tbAlgo) (void * tab);

// Global variables
extern unsigned int randomSeed;
extern int dataSize;

// Local variables
void * ModuleHandler;

// Local functions
void TestbedUsage ();
int main (int argc, char * argv []);


#endif /* TESTBED_H_ */
