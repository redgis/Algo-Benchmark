/*
 * testbed.cpp
 *
 *  Created on: May 12, 2010
 *      Author: regis
 */

#include "testbed.h"

unsigned int randomSeed;
int dataSize;

void (* tbInitialize) ();
void * (* tbCreateData) (int argc, char * argv[]);
bool (* tbValidate) (void *);
void (* tbUsage) ();
void * (* tbAlgo) (void * tab);


/*****************************************************************************/
void TestbedUsage ()
{
  cout << "testbed --help                                    Display this help" << endl;
  cout << "testbed --help [Module]                           Display help for [Algo]" << endl;
  cout << "testbed [Module] [Algo] [Data size] [Nb tests] [Random Seed] [Algo parameters]" << endl;
}

/*****************************************************************************/
void LoadModule (const char * ModuleName, const char * AlgoName)
{
  // Open module
  ModuleHandler = dlopen ((string(ModuleName)+".so").c_str(), RTLD_LAZY|RTLD_LOCAL);
  if (ModuleHandler == NULL)
  {
    cerr << "ERROR : MAIN : cannot open module " << ModuleName << endl;
    cerr << dlerror() << endl;
    exit (1);
  }
  cout << "SUCCESS : Loaded " << ModuleName << endl;

  // Load algorithm
  if (AlgoName != NULL)
  {
    tbAlgo = (void * (*) (void*)) dlsym(ModuleHandler, AlgoName);
    if (tbAlgo == NULL)
    {
      cerr << "ERROR : MAIN : cannot load algorithm [" << AlgoName << "] in [" << ModuleName << "]" << endl;
      cerr << dlerror() << endl;
      dlclose (ModuleHandler);
      exit (1);
    }
    cout << "SUCCESS : Loaded [" << AlgoName << "] from " << ModuleName << "]" << endl;
  }

  // Load Initialize ()
  tbInitialize = (void (*) ()) dlsym (ModuleHandler, "Initialize");
  if (tbInitialize == NULL)
  {
    cerr << "ERROR : MAIN : cannot load [Initialize] function in ["<< ModuleName << "]" << endl;
    cerr << dlerror() << endl;
    dlclose (ModuleHandler);
    exit (1);
  }
  cout << "SUCCESS : Loaded [Initialize] from [" << ModuleName << "]"<< endl;

  // Load CreateData ()
  tbCreateData = (void * (*) (int,char**)) dlsym(ModuleHandler, "CreateData");
  if (tbCreateData == NULL)
  {
    cerr << "ERROR : MAIN : cannot load [CreateData] function in [" << ModuleName << "]" << endl;
    cerr << dlerror() << endl;
    dlclose (ModuleHandler);
    exit (1);
  }
  cout << "SUCCESS : Loaded [CreateData] from [" << ModuleName << "]" << endl;

  // Load Validate ()
  tbValidate = (bool (*) (void*)) dlsym(ModuleHandler, "Validate");
  if (tbValidate == NULL)
  {
    cerr << "ERROR : MAIN : cannot load [Validate] function in [" << ModuleName << "]" << endl;
    cerr << dlerror() << endl;
    dlclose (ModuleHandler);
    exit (1);
  }
  cout << "SUCCESS : Loaded [Validate] from [" << ModuleName << "]" << endl;

  // Load Usage ()
  tbUsage = (void (*) ()) dlsym(ModuleHandler, "Usage");
  if (tbUsage == NULL)
  {
    cerr << "ERROR : MAIN : cannot load [Usage] function in [" << ModuleName << "]" << endl;
    cerr << dlerror() << endl;
    dlclose (ModuleHandler);
    exit (1);
  }
  cout << "SUCCESS : Loaded [Usage] from [" << ModuleName << "]" << endl;
}

/*****************************************************************************/
void CloseModule ()
{
  dlclose (ModuleHandler);
}

/*****************************************************************************/
int main (int argc, char * argv [])
{
  const char * ModuleName;
  const char * AlgoName;
  int NbTests;
  int NbAlgoParams;
  char ** AlgoParams = NULL;

  void ** DataSets;
  void ** ResultSets;

  // Timers
  struct timeval totalStart;
  struct timeval totalEnd;
  struct timeval testStart;
  struct timeval testEnd;

  double * testTimes;
  double totalTime;

  // Process parameters
  if (argc == 2) {                       // testbed --help
    TestbedUsage ();
    exit(0);
  } else if (argc == 3) {                // testbed --help [Module]
    if (strcmp (argv[1], "--help") == 0)
    {
      LoadModule (argv[2], NULL);
      (*tbUsage) ();
      exit (0);
    }
    else
    {
      TestbedUsage();
      exit(1);
    }
  } else if (argc >= 6) {                // testbed [Module] [Algo] [Data size] [Nb tests] [Random Seed] [Algo parameters]
    ModuleName = argv[1];
    AlgoName = argv[2];
    dataSize = atoi (argv[3]);
    NbTests = atoi (argv[4]);
    randomSeed = (unsigned int) atoi (argv[5]);
    NbAlgoParams = argc-6;
    if (NbAlgoParams > 0)
      AlgoParams = argv+6;
  } else {
    TestbedUsage ();
    exit (0);
  }

  // Load test module
  LoadModule (ModuleName, AlgoName);

  // Initialize module
  (*tbInitialize) ();

  // Create X instances of test data (given in parameter)
  DataSets = new void* [NbTests];
  ResultSets = new void* [NbTests];
  for (int Index = 0; Index < NbTests; Index++)
  {
    DataSets[Index] = (*tbCreateData) (NbAlgoParams, AlgoParams);
    ResultSets[Index] = NULL;
  }

  // Initialize timers
  testTimes = new double [NbTests];

  for (int Index = 0; Index < NbTests; Index ++)
  {

    testTimes[Index] = 0;
  }

  gettimeofday(&totalStart, NULL);

  // Execute algorithm
  for (int Index = 0; Index < NbTests; Index++)
  {
    cout << Index << " " << flush;

    gettimeofday(&testStart, NULL);
    ResultSets[Index] = (*tbAlgo) (DataSets[Index]);
    gettimeofday(&testEnd, NULL);

    testTimes[Index] = ( (double)(testEnd.tv_sec - testStart.tv_sec) ) + \
            ( ((double)testEnd.tv_usec/(double)1000000) - ((double)testStart.tv_usec/(double)1000000) );
  }

  gettimeofday(&totalEnd, NULL);
  totalTime = (double)(totalEnd.tv_sec - totalStart.tv_sec) + \
      (((double)totalEnd.tv_usec/(double)1000000) - ((double)totalStart.tv_usec/(double)1000000));

  cout << endl;

  int NbValid = 0;
  // Validate algorithm result
  for (int Index = 0; Index < NbTests; Index++)
  {
    NbValid += (*tbValidate) (ResultSets[Index]);
  }

  // Write statistics
  cout << "     " << "\t" \
       << "Tests" << "\t" \
       << "Valid" << "\t" \
       << "Size" << "\t" \
       << "Seed" << "\t\t" \
       << "Time" << "\t\t" \
       << "MeanTime" << "\t" \
       << "StdDev" << endl;


  cout.setf (ios_base::fixed);

  cout << "Resume:\t"\
       << NbTests << "\t" \
       << NbValid << "\t" \
       << dataSize << "\t" \
       << randomSeed << "\t" \
       << totalTime << "\t" \
       << (double)totalTime / (double) NbTests << "\t" \
       << (double) gsl_stats_sd (testTimes, 1, NbTests) \
       << endl;

  for (int Index = 0; Index < NbTests; Index++)
  {
    cout << "Test." << Index << "\t" << testTimes[Index] << endl;
  }


  //cout << "Per_test_average_time " << (double)TotalTimer.tv_sec / (double) NbTests << " s  " << ((unsigned long)TotalTimer.tv_nsec / (unsigned long) NbTests) / (double) 1000000 << " msec" << endl;

  // Clean up
  CloseModule ();
  // fuck the free (), don't care : we are exiting anyway !

  return 0;
}

