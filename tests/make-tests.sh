#!/bin/sh

#List of modules to test
Modules="sort"

#list of algorithms to test
Algorithms="Quick_Sort Insertion_Sort Selection_Sort Bubble_Sort Shell_Sort" # Fusion_Sort

#List of data size to test with
DataSize="100 1000 2000 3000 4000 5000 6000 7000 8000 9000 10000 20000 30000 40000 50000 60000 70000 80000 90000 100000"

#Number of trials for each condition
Trials=100

#Random seed. Maybe you want it to be always the same
Seed="`date +%s`"


for Module in $Modules
do
  for Size in $DataSize
  do
    for Algo in $Algorithms
    do
      for AlgoParams in `seq 0 0.025 0.1 ; seq 0.15 0.05 0.9 ; seq 0.925 0.025 1.0`
      do
  #      echo "$Module $Algo $Size $Trials $Seed $AlgoParams"
        Result=`./testbed $Module $Algo $Size $Trials $Seed $AlgoParams | grep Resume`
        Result=`echo $Result $Algo $AlgoParams | cut -d " " -f "4,7,8,9,10"`
        echo $Result >> $Module/$Module-db.txt
      done
    done
  done

  cd $Module ; ./update-db.sh ; ./make-plots.sh ; cd ..
done
