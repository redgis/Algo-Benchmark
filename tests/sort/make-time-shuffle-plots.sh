#!/bin/bash

rm -fr time-shuffle plots/time-shuffle* figures/time-shuffle*
mkdir time-shuffle


Algos=`sqlite3 -separator ' ' data.db "select distinct Algo from sorts"`
#Sizes=`sqlite3 -separator ' ' data.db "select distinct Size from sorts"`
Sizes=`sqlite3 -separator ' ' data.db "select distinct Size from sorts where Size<=10000"`


############ 1- GENERATE .TXT DATA FILES

for Algo in $Algos
do
  for Size in $Sizes
  do
    # Make data file
    sqlite3 -separator ' ' data.db "select Shuffle,MeanTime,StdDev from sorts where Algo='$Algo' and Size='$Size' order by Shuffle" > time-shuffle/$Algo-$Size.txt
  done
done



########### 2- GENERATE .PLOT SCRIPTS

# Per sizes graphs (several algo curves ont each plot)
for Size in $Sizes
do
  # Make plot script
  cat ../plot-header.txt >> plots/time-shuffle-$Size.plot
  echo -n "
  #########################################
  set title '$Size - Computing Time vs Randomization'
  set output 'figures/time-shuffle-$Size.eps'
  set ylabel 'Time [seconds]'
  set xlabel 'Shuffle (0 = sorted, 1 = random order)'  
  plot x/0 w p notitle" >> plots/time-shuffle-$Size.plot

  linestyle=0

  for Algo in $Algos
  do
      
    echo -n ", \\
    'time-shuffle/$Algo-$Size.txt' using 1:2:3 title 'Algo = $Algo' w l ls $linestyle, \\
    'time-shuffle/$Algo-$Size.txt' using 1:2:3 notitle w yerrorbars ls 1" >> plots/time-shuffle-$Size.plot
    
    linestyle=$(($linestyle+1))
  done
done

# Per Algor graphs (several data size curves ont each plot)
for Algo in $Algos
do
  # Make plot script
  cat ../plot-header.txt >> plots/time-shuffle-$Algo.plot
  echo -n "
  #########################################
  set title '$Algo - Computing Time vs Randomization'
  set output 'figures/time-shuffle-$Algo.eps'
  set ylabel 'Time [seconds]'
  set xlabel 'Shuffle (0 = sorted, 1 = random order)'  
  plot x/0 w p notitle" >> plots/time-shuffle-$Algo.plot

  linestyle=0

  for Size in $Sizes
  do
    echo -n ", \\
    'time-shuffle/$Algo-$Size.txt' using 1:2:3 title 'N = $Size' w l ls $linestyle, \\
    'time-shuffle/$Algo-$Size.txt' using 1:2:3 notitle w yerrorbars ls 1" >> plots/time-shuffle-$Algo.plot
    
    linestyle=$(($linestyle+1))
  done
done

gnuplot plots/time-shuffle*.plot ; evince figures/time-shuffle*.eps

