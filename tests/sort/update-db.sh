#!/bin/bash

############ Update the table for sorts tests
sqlite3 data.db "drop table sorts"

sqlite3 data.db "create table sorts ( \
Size INTEGER, \
MeanTime REAL, \
StdDev REAL, \
Algo TEXT, \
Shuffle REAL )"

sqlite3 -separator ' ' data.db ".import sort-db.txt sorts"

