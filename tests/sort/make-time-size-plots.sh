#!/bin/bash

rm -fr time-size plots/time-size* figures/time-size*
mkdir time-size



Algos=`sqlite3 -separator ' ' data.db "select distinct Algo from sorts"`
#Shuffles=`sqlite3 -separator ' ' data.db "select distinct Shuffle from sorts"`
Shuffles=`sqlite3 -separator ' ' data.db "select distinct Shuffle from sorts where Shuffle=0 or Shuffle=0.5 or Shuffle=1"`


############ 1- GENERATE .TXT DATA FILES
for Algo in $Algos
do
  #for Shuffle in `sqlite3 -separator ' ' data.db "select distinct Shuffle from sorts"`
  for Shuffle in $Shuffles
  do
    # Make data file
    sqlite3 -separator ' ' data.db "select Size,MeanTime,StdDev from sorts where Algo='$Algo' and Shuffle='$Shuffle' and Size<=10000 order by Size" > time-size/$Algo-$Shuffle.txt
#    sqlite3 -separator ' ' data.db "select Size,MeanTime,StdDev from sorts where Algo='$Algo' and Shuffle='$Shuffle' order by Size" > time-size/$Algo-$Shuffle.txt     
  done
done


########### 2- GENERATE .PLOT SCRIPTS

# Per sizez graphs (several algo curves ont each plot)
for Shuffle in $Shuffles
do
  # Make plot script
  cat ../plot-header.txt >> plots/time-size-$Shuffle.plot
  echo -n "
  #########################################
  set title '$Shuffle - Computing Time vs Data Size'
  set output 'figures/time-size-$Shuffle.eps'
  set ylabel 'Time [seconds]'
  set xlabel 'Size of input data'  
  plot x/0 w p notitle" >> plots/time-size-$Shuffle.plot

  linestyle=0

  for Algo in $Algos
  do
      
    echo -n ", \\
    'time-size/$Algo-$Shuffle.txt' using 1:2:3 title 'Algo = $Algo' w l ls $linestyle, \\
    'time-size/$Algo-$Shuffle.txt' using 1:2:3 notitle w yerrorbars ls 1" >> plots/time-size-$Shuffle.plot
    
    linestyle=$(($linestyle+1))
  done
done

# Per Algor graphs (several data size curves ont each plot)
for Algo in $Algos
do
  # Make plot script
  cat ../plot-header.txt >> plots/time-size-$Algo.plot
  echo -n "
  #########################################
  set title '$Algo - Computing Time vs Data Size'
  set output 'figures/time-size-$Algo.eps'
  set ylabel 'Time [seconds]'
  set xlabel 'Size of input data'  
  plot x/0 w p notitle" >> plots/time-size-$Algo.plot

  linestyle=0

  for Shuffle in $Shuffles
  do
    echo -n ", \\
    'time-size/$Algo-$Shuffle.txt' using 1:2:3 title 'N = $Shuffle' w l ls $linestyle, \\
    'time-size/$Algo-$Shuffle.txt' using 1:2:3 notitle w yerrorbars ls 1" >> plots/time-size-$Algo.plot
    
    linestyle=$(($linestyle+1))
  done
done


gnuplot plots/time-size*.plot ; evince figures/time-size*.eps

