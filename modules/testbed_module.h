/*
 * testbed_module.h
 *
 *  Created on: May 12, 2010
 *      Author: regis
 */

#ifndef TESTBED_MODULE_H_
#define TESTBED_MODULE_H_

extern unsigned int randomSeed;
extern int dataSize; // In fact, the "n" of the complexity

extern "C"
{
// Module Interface
void Initialize ();
bool Validate (void * tab);
void * CreateData (int argc, char * argv[]);
void Usage ();
}

#endif /* TESTBED_MODULE_H_ */
