/*
 * sort.cpp
 *
 *  Created on: May 12, 2010
 *      Author: regis
 */


#include "sort.h"

extern "C"
{

typedef int datatype;

/*****************************************************************************/
// Initialization function
 void Initialize ()
{
  // Initialize random generator
  srand (randomSeed);
}

/*****************************************************************************/
// Result validation
bool Validate (void * t)
{
  datatype * tab = (datatype * ) t;

  bool result = true;

  for (int Index = 1; Index < dataSize; Index++)
  {
    if (tab[Index - 1] > tab[Index])
    {
      result = false;
      break;
    }
  }

  return result;
}

/*****************************************************************************/
// Display usage
void Usage ()
{
  cout << "SORT Usage : [float shuffle]" << endl;
}

// Generates a tab of int, randomly chosen between
// 3 Expected parameters : int size, float shuffle
void * CreateData (int argc, char * argv[])
{
  if (argc != 1)
  {
    cerr << "ERROR : SORT : Missing additional parameters to command line." << endl;
    Usage ();
    exit (1);
  }

  // Make my life easier
  double shuffle = (double) atof (argv[0]);

  datatype * resultTab = new datatype[dataSize];

  // create initially perfectly sorted tab
  for (int Index = 0; Index < dataSize; Index ++)
  {
    resultTab[Index] = Index;
  }


  // reshuffle the tab based on "suffle" parameter
  for (int Index = 0; Index < dataSize; Index++)
  {
    //principle : for each element, exchange it with an arbitrary other position, with probability "shuffle"

    bool change = ( (((double)rand ()) / (double)RAND_MAX) <= shuffle );

    int newPos;
    datatype Temp;

    if (change)
    {
      // Choose swap position and do th swap
      newPos = ( ((double)rand ()) / ((double)RAND_MAX) ) * (double) dataSize;
      Temp = resultTab[newPos];
      resultTab[newPos] = resultTab[Index];
      resultTab[Index] = Temp;
    }
  }


  return (void *) resultTab;
}


/*****************************************************************************/
// Algo available for testbed

/*****************************************************************************/
void * Fusion_Sort (void * tab)
{

  return tab;
}

/*****************************************************************************/
void * Quick_Sort (void * t)
{
  datatype * tab = (datatype *) t;

  int backupSize = dataSize;

  int g,d;
  int left = 0;
  int right = dataSize-1;
  datatype Temp;
  datatype val;

  if(dataSize <= 1) /* fin de récursivité si tableau d'une seule case à trier */
  {
    return t;       /* choix du pivot : on prend par exemple la valeur de droite */
  }

  val = tab[right];
  g = left-1;
  d = right;

  do
  {
    while ((g<d) && (tab[++g] < val)); /* g pointe le premier élément (à gauche) plus grand (ou égal) que le pivot */
      //g++;
    while ((g<d) && (tab[--d] > val)); /* d pointe le premier élément (par la droite) plus petit (ou égal) que le pivot */
      //d--;

    if (g<d) /* si g et d ne se sont pas rencontrés, on échange les contenus de g et d et on recommence */
    {
      Temp = tab[g];
      tab[g] = tab[d];
      tab[d] = Temp;
    }
  }
  while (g<d); /* on sort quand g a rencontré d, alors tous les éléments à gauche de g sont <= au pivot, tous ceux à droite de d sont >= */

  /* on place le pivot en position g (d serait aussi possible), donc dans sa bonne position (tous ceux à gauche sont <=, à droite sont >=) */
  Temp = tab[g];
  tab[g] = tab[right];
  tab[right] = Temp;

  /* il ne reste plus qu'à trier les deux parties, à droite et à gauche du pivot */

  dataSize = g;
  Quick_Sort (&(tab[left]));

  dataSize = right - g;
  Quick_Sort (&(tab[g+1]));

  dataSize = backupSize;

  return t;
}

/*****************************************************************************/
void * Insertion_Sort (void * t)
{
  datatype * tab = (datatype*) t;

  int pt, dpg; /* position testée, dernier plus grand */
  datatype Temp;

  for(pt = 1; pt < dataSize; pt++)
  {
    dpg = pt-1;
    Temp = tab[pt];

    while( (tab[dpg] > Temp) && (dpg >= 0) )
    {
      tab[dpg+1] = tab[dpg];
      dpg--;
    }
    tab[dpg+1] = Temp;
  }

  /*
  for(pt=1;pt<N;pt++)
  {
    ppg=0;
    while(tab[ppg]<=tab[pt]&&ppg<pt)ppg++;
    if(ppg<pt)
    {
      tampon=tab[pt];
      suppr_tus(tab,&N,pt);
      insert_tus(tab,&N,32767,ppg,tampon); // je suis sur de ne pas dépasser la dimension puisque je viens de supprimer un élément
    }
  }
  */

  return t;
}

/*****************************************************************************/
void * Shell_Sort (void * t)
{
  datatype * tab = (datatype*) t;

  int pt,dpg,P; // position testée,dernier plus grand
  datatype Temp;

  for(P = 1; P <= dataSize / 9; P = 3 * P+1); // calcul de P initial (<=N/9)
  for(; P > 0; P /= 3) // inutile de soustraire 1 car division entière
  {
    for(pt=P; pt < dataSize; pt++)
    {
      dpg = pt-P;
      Temp = tab[pt];

      while((tab[dpg] > Temp) && (dpg >= P-1) )
      {
        tab[dpg+P] = tab[dpg];
        dpg -= P;
      }

      tab[dpg+P] = Temp;
    }
  }


  return t;
}

/*****************************************************************************/
void * Selection_Sort (void * t)
{
  datatype * tab = (datatype*) t;

  int pd, pp, i; /* place définitive, plus petit */
  datatype Temp;

  for (pd = 0; pd < dataSize-1; pd++)
  {
    pp = pd;
    for (i = pp+1; i < dataSize; i++)
      if (tab[i] < tab[pp])
        pp = i;

    Temp = tab[pp];
    tab[pp] = tab[pd];
    tab[pd] = Temp;
  }



  return t;
}

/*****************************************************************************/
void * Bubble_Sort (void * t)
{
  bool Finished;
  datatype * tab = (datatype *) t;

  datatype Temp;

  do
  {
    Finished = true;

    for(int Index = 1; Index < dataSize; Index++)
    {
      if (tab[Index-1] > tab[Index]) // If adjacent indexes are not sorted
      {
        Finished = false;
        Temp = tab[Index-1];
        tab[Index-1] = tab[Index];
        tab[Index] = Temp;
      }
    }
  }
  while(!Finished);

  return t;
}
/*****************************************************************************/


} // extern C
