/*
 * sort.h
 *
 *  Created on: May 12, 2010
 *      Author: regis
 */

#ifndef SORT_H_
#define SORT_H_

#include "testbed_module.h"

// C++
#include <iostream>

// ANSI C
#include <stdlib.h>

using namespace std;

extern "C"
{

// Algo available for testbed
void * Fusion_Sort (void * tab);
void * Quick_Sort (void * tab);
void * Insertion_Sort (void * tab);
void * Selection_Sort (void * tab);
void * Bubble_Sort (void * tab);
void * Shell_Sort (void * tab);
}

#endif /* SORT_H_ */
