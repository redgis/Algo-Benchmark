# Test bed

Basic performance benchmarking for algorithms. It loads algorithms as independant modules, thus anybody can implement its own algorythms as shared object modules and benchmark them easily. Follow the "sort" module example.


## Algo tests : the sort algorithms

_A comparison of different implementations of **sort** algorithms._

I recently came to realize (during a job opportunity interview :/) that I didn't remember much about the underlying implications of the different sort algorithms. I ended up reading a couple of nice (french) web pages on the topic. Mainly [this one](http://bigocheatsheet.com/) and [this one](http://lwh.free.fr/pages/algo/tri/tri.htm). I implemented different methods in my benchmark, namely : bubble sort, insertion sort, quick sort, selection sort and shell sort. Didn't have time to implement any other sort algo, although there are many.

### Calculation time versus data sample size

These results are the mean values on 100 trials. The sort algorithms are run on a completely randomized data set (i.e. all elements of the data sample are randomly permutated with another).

![](documentation/time-size-1.0.png)

Here is a detailed view of the performance evolution with data size, for each algorithm. */!\\ Beware of the axis scales /!\\*

|  Bubble sort  |  Insertion sort  |  Quick sort  |  Selection sort  |  Shell sort  |
|---------------|------------------|--------------|------------------|--------------|
|  ![Bubble sort](documentation/time-size-bubble_sort.png)  |  ![Insertion sort](documentation/time-size-insertion_sort.png) | ![Quick sort](documentation/time-size-quick_sort.png) | ![Selection sort](documentation/time-size-selection_sort.png) | ![Shell sort](documentation/time-size-shell_sort.png) |


### Calculation time versus shuffling probability

We here ran the algorithms on a sample of size 10000. The varying condition is the shuffling of the data sample. With a suffle probability of 0.0, the samples are fully ordered. Than moving toward a probability of 1.0, there are more and more samples that are not in their right place. 

![](documentation/time-shuffle-10000.png)


